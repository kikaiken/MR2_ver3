/*
 * light_math.h
 *
 *  Created on: 2019/02/27
 *      Author: nabeya11
 */

#ifndef LIGHT_MATH_H_
#define LIGHT_MATH_H_

//マクロ
#define DEG2RAD(x)			(float)((x) * M_PI / 180)
#define RAD2DEG(x)			(float)((x) * 180 / M_PI)

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))


//プロトタイプ宣言
float signf(float a);

#endif /* LIGHT_MATH_H_ */
