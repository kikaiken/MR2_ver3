/*
 * uart_terminal.h
 *
 *  Created on: 2019/01/02
 *      Author: Ryohei
 */

#ifndef UART_TERMINAL_H_
#define UART_TERMINAL_H_


RET UartTerminalInit(UART_HandleTypeDef *huart);
RET UartTerminalSend(uint8_t data);
uint8_t UartTerminalRecv();
RET UartTerminalRecvTry(uint8_t *data);


#endif /* UART_TERMINAL_H_ */
