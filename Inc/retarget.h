/*
 * retarget.h
 *
 *  Created on: 2019/01/02
 *      Author: Ryohei
 */

#ifndef RETARGET_H_
#define RETARGET_H_


#include "usart.h"

//プロトタイプ宣言
void RetargetInit(void);

//UARTハンドラ
#define UART_HANDLER		huart1

#endif /* RETARGET_H_ */
