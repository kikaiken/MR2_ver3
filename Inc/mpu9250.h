/*
 * gyro.h
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#ifndef MPU9250_H_
#define MPU9250_H_


#include "stm32f4xx_hal.h"
#include "spi.h"

//SPI通信ハンドラ
#define MPU9250_SPI_HANDLER			hspi1

//CSポート、ピン
#define MPU9250_CS_PORT				GPIOA
#define MPU9250_CS_PIN				GPIO_PIN_4

//レジスタ
#define MPU9250_WHO_AM_I			0x75
#define MPU9250_SMPLRT_DIV			0x19
#define MPU9250_CONFIG				0x1A
#define MPU9250_GYRO_CONFIG			0x1B
#define MPU9250_ACCEL_CONFIG		0x1C
#define MPU9250_ACCEL_CONFIG_2		0x1D
#define MPU9250_INT_PIN_CFG			0x37
#define MPU9250_ACCEL_XOUT_H		0x3B
#define MPU9250_ACCEL_XOUT_L		0x3C
#define MPU9250_ACCEL_YOUT_H		0x3D
#define MPU9250_ACCEL_YOUT_L		0x3E
#define MPU9250_ACCEL_ZOUT_H		0x3F
#define MPU9250_ACCEL_ZOUT_L		0x40
#define MPU9250_TEMP_OUT_H			0x41
#define MPU9250_TEMP_OUT_L			0x42
#define MPU9250_GYRO_XOUT_H			0x43
#define MPU9250_GYRO_XOUT_L			0x44
#define MPU9250_GYRO_YOUT_H			0x45
#define MPU9250_GYRO_YOUT_L			0x46
#define MPU9250_GYRO_ZOUT_H			0x47
#define MPU9250_GYRO_ZOUT_L			0x48
#define MPU9250_PWR_MGMT_1      	0x6B
#define MPU9250_PWR_MGMT_2      	0x6C

//データ構造体
typedef struct {
	float acc_x;
	float acc_y;
	float acc_z;
	float angvel_x;
	float angvel_y;
	float angvel_z;
	float temp;
} imu_s;

typedef enum {
	MPU9250_ACC_2G  = 0b00,
	MPU9250_ACC_4G  = 0b01,
	MPU9250_ACC_8G  = 0b10,
	MPU9250_ACC_16G = 0b11
} acc_range_s;

typedef enum {
	MPU9250_GYRO_250DPS  = 0b00,
	MPU9250_GYRO_500DPS  = 0b01,
	MPU9250_GYRO_1000DPS = 0b10,
	MPU9250_GYRO_2000DPS = 0b11
} gyro_range_s;

//プロトタイプ宣言
void MPU9250GetData(imu_s *);
void MPU9250GetOffset(void);
int8_t MPU9250Init(gyro_range_s, acc_range_s);
void MPU9250Deselect(void);


#endif /* MPU9250_H_ */
