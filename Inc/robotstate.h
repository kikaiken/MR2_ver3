/*
 * robotstate.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef ROBOTSTATE_H_
#define ROBOTSTATE_H_


#include "stm32f4xx_hal.h"
#include "mpu9250.h"

typedef struct {
	float x;
	float y;
	float theta;
} pos_s;

typedef struct {
	float roll;
	float pitch;
	float yaw;
} angle_s;

typedef enum {
	MM_START_TO_SANDDUNE,
	MM_START_TO_SANDDUNE_PREPARE,

	MM_SANDDUNE,
	MM_SANDDUNE_PREPARE,

	MM_SANDDUNE_TO_TUSSOCK,
	MM_SANDDUNE_TO_TUSSOCK_PREPARE,

	MM_TUSSOCK,
	MM_TUSSOCK_PREPARE,

	MM_MOUNTAIN,
	MM_MOUNTAIN_PREPARE,

	MM_STOP,
} move_mode_e;

typedef enum {
	FIELD_RED,
	FIELD_BLUE
} field_select_e;

typedef struct {
	volatile move_mode_e move_mode;

	//機体姿勢
	volatile float vel;						//速さ
	volatile float angvel;					//角速度
	volatile float angvel_error_integ;
	volatile angle_s angle;

	//機体位置
	volatile pos_s odom;
} state_s;


extern state_s state;
extern float log4debug[1000];


//プロトタイプ宣言
void GetOdometry(imu_s);
void SetMoveMode(move_mode_e);

#endif /* ROBOTSTATE_H_ */
