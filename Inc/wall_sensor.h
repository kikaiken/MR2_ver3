/*
 * wall_sensor.h
 *
 *  Created on: 2019/03/03
 *      Author: Ryohei
 */

#ifndef WALL_SENSOR_H_
#define WALL_SENSOR_H_

#include "stm32f4xx_hal.h"
#include "adc.h"

//ADCハンドラ
#define WALL_SENSOR_ADC_HANDLER		hadc1

//構造体
typedef struct {
	float front_left;
	float front_right;
	float left_front;
	float left_rear;
	float right_front;
	float right_rear;
	float rear_left;
	float rear_right;
} wall_sens_s;

typedef struct {
	float side_wall_error;
	float front_wall_dist;
	float front_wall_angle;
	float rear_wall_dist;
	float rear_wall_angle;
} wall_pos_s;

//プロトタイプ宣言
void GetPosFromWall(wall_pos_s *, wall_sens_s);
void WallSensorGetDist(wall_sens_s *);
void WallSensorEnable(void);
void WallSensorDisable(void);

#endif /* WALL_SENSOR_H_ */
