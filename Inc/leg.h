/*
 * motor.hpp
 *
 *  Created on: 2018/05/23
 *      Author: Ryohei
 */

#ifndef LEG_H_
#define LEG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"
#include "tim.h"
#include "spi.h"
#include "wall_sensor.h"

/*-----------------------------------------------
 * 左右モータ
 ----------------------------------------------*/
#define H_TIM_MOTOR				htim8				//TIMx_HandleTypeDef
#define TIM_MOTOR_L_CH			TIM_CHANNEL_2
#define TIM_MOTOR_R_CH			TIM_CHANNEL_1
#define TIM_MOTOR_FREQ			72000000
#define MOTOR_PWM_FREQ			10000
#define MOTOR_DUTY_LIMIT		0.7f

//GPIO
#define MOTOR_L1_PORT			GPIOB
#define MOTOR_L1_PIN			GPIO_PIN_12
#define MOTOR_L2_PORT			GPIOB
#define MOTOR_L2_PIN			GPIO_PIN_13

#define MOTOR_R1_PORT			GPIOB
#define MOTOR_R1_PIN			GPIO_PIN_10
#define MOTOR_R2_PORT			GPIOB
#define MOTOR_R2_PIN			GPIO_PIN_11

//プロトタイプ宣言
void FrontWallStick(wall_pos_s, float, float);
void BackWallStick(wall_pos_s, float, float);
void MotorSetSpdDutyAngvel(float, float);
void MotorSetSpdDutyAngvel4Sanddune(float, float);
void MotorSetSpdTurnDuty(float, float);
void MotorBreak(void);
void MotorDisable(void);


/*-----------------------------------------------
 * エンコーダ
 ----------------------------------------------*/
#define AMT203_SPI_HANDLER			hspi3

#define AMT203_LEFT_CS_PORT			GPIOA
#define AMT203_LEFT_CS_PIN			GPIO_PIN_14
#define AMT203_RIGHT_CS_PORT		GPIOA
#define AMT203_RIGHT_CS_PIN			GPIO_PIN_15

typedef enum {
	AMT203_LEFT,
	AMT203_RIGHT
} amt203_select_e;

#define	AMT203_NOP_A5				0x00
#define AMT203_RD_POS				0x10
#define AMT203_SET_ZERO_POINT		0x70

//プロトタイプ宣言
int8_t AMT203Init(void);
int8_t LegGetData(void);
int8_t LegGetPhaseDeg(int32_t *, float *, int32_t *, float *);
void AMT203Select(amt203_select_e);
void AMT203Deselect(void);


#ifdef __cplusplus
}
#endif
#endif /* LEG_H_ */
