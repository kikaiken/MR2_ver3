/*
 * gerge.h
 *
 *  Created on: 2018/12/23
 *      Author: Ryohei
 */

#ifndef GERGE_H_
#define GERGE_H_

#include "stm32f4xx_hal.h"
#include "tim.h"

//プロトタイプ宣言
void GergeCatchStart(void);
void GergeCatchAndWaitButton(void);
void GergeUukhai(void);
void GergeConfirmLock(void);

void GergeMotorSetDuty(float duty);
void GergeMotorEnable(void);
void GergeMotorDisable(void);

void GergeHandServoTransfer(void);
void GergeHandServoUukhai(void);

void GergeLock(void);
void GergeUnLock(void);

void GergeHandClose(void);
void GergeHandOpen(void);

int8_t GergeIsToCatch(void);

//レールDCモータ
#define GERGE_MOTOR_TIM_HANDLER			htim8
#define GERGE_MOTOR_TIM_CH				TIM_CHANNEL_3
#define GERGE_MOTOR_DIR_1_PORT			GPIOB
#define GERGE_MOTOR_DIR_1_PIN			GPIO_PIN_14
#define GERGE_MOTOR_DIR_2_PORT			GPIOB
#define GERGE_MOTOR_DIR_2_PIN			GPIO_PIN_15
//PWM周波数
#define GERGE_PWM_FREQ					10000
//タイマ周波数
#define GERGE_TIM_FREQ					72000000
//Duty比のリミット
#define GERGE_DUTY_LIMIT				1.00f

//サーボモータ
#define GERGE_SERVO_TIM_HANDLER			htim3
#define GERGE_HAND_SERVO_TIM_CH			TIM_CHANNEL_2
#define GERGE_LOCK_SERVO_TIM_CH			TIM_CHANNEL_1

#define GERGE_HAND_SERVO_CENTER_VALUE	1350
#define GERGE_HAND_SERVO_1DEG_VALUE		11.3

#define GERGE_LOCK_VALUE				2150
#define GERGE_UNLOCK_VALUE				1000

//ハンドエアシリンダ
#define GERGE_HAND_CYLINDER_PORT		GPIOA
#define GERGE_HAND_CYLINDER_PIN			GPIO_PIN_11

//接触センサ
#define GERGE_TOUCH_SW_PORT				GPIOB
#define GERGE_TOUCH_SW_PIN				GPIO_PIN_6


#endif /* GERGE_H_ */
