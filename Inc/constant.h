/*
 * constant.h
 *
 *  Created on: 2019/01/13
 *      Author: Ryohei
 */

#ifndef CONSTANT_H_
#define CONSTANT_H_


#define CTRL_CYCLE					0.01f		//制御周期
#define GRAVITATIONAL_ACC			9.807f		//重力加速度

//エラー
#define MY_ERROR					1
#define MY_ERROR_NONE				0

//フィールドデータ
#define FIELD_WIDTH_UR1_TO_SAND		1.90f		//ウルトゥー1から砂丘の間の左右壁の幅

//機体パラメータ
#define MR2_LENGTH
#define MR2_WIDTH
#define MR2_HEIGHT
#define MR2_STEP_LENGTH				0.10f		//歩幅
#define MR2_TREAD					0.3756f		//脚のトレッド

//PSD距離センサの前後・左右の距離
#define MR2_SENSOR_FLFR				0.18f		//FLとFR距離
#define MR2_SENSOR_RLRR				0.18f
#define MR2_SENSOR_LFRF				0.57f


#endif /* CONSTANT_H_ */
