/*
 * user_interface.h
 *
 *  Created on: 2018/05/20
 *      Author: Ryohei
 */

#ifndef USER_INTERFACE_H_
#define USER_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "stm32f4xx_hal.h"
#include "robotstate.h"

/*-----------------------------------------------
 * マイクロ秒タイマ
 ----------------------------------------------*/
/*
//タイマハンドラ
#define MICROSEC_TIM_HANDLER	htim10

//プロトタイプ宣言
void DelayMicroSec(uint32_t);
*/

/*-----------------------------------------------
 * LED・ブザー表示パターン
 ----------------------------------------------*/
//プロトタイプ宣言
void UIWarning(void);
void UIModeExec(field_select_e);
void UIModeBack(void);
void UIStartUp(void);
void UIUukhai(void);

/*-----------------------------------------------
 * UILED
 ----------------------------------------------*/
//UILEDのポート、ピン
#define UILED_BIT1_PORT		GPIOC
#define UILED_BIT1_PIN		GPIO_PIN_0

#define UILED_BIT2_PORT		GPIOC
#define UILED_BIT2_PIN		GPIO_PIN_1

#define UILED_BIT3_PORT		GPIOC
#define UILED_BIT3_PIN		GPIO_PIN_2

#define UILED_BIT4_PORT		GPIOC
#define UILED_BIT4_PIN		GPIO_PIN_3

//プロトタイプ宣言
void UILEDSet(uint8_t);
void UILEDToggle(uint8_t);

/*-----------------------------------------------
 * Buzzer
 ----------------------------------------------*/
//ブザーのポート、ピン
#define BUZZER_PORT			GPIOA
#define BUZZER_PIN			GPIO_PIN_13

//プロトタイプ宣言
void BuzzerBeep(int32_t);
void BuzzerOn(void);
void BuzzerOff(void);

/*-----------------------------------------------
 * Button
 ----------------------------------------------*/
//ON/OFF/長押し/短押し
typedef enum {
	BUTTON_NUM_ERROR = -1,
	BUTTON_OFF = 0,
	BUTTON_ON,
	BUTTON_LONG,
	BUTTON_SHORT,
	BUTTON_PULL_UP,
	BUTTON_PULL_DOWN
} button_e;

//Buttonのポート、ピン
#define BUTTON_1_PORT			GPIOB
#define BUTTON_1_PIN			GPIO_PIN_8
//#define BUTTON_1_PULL_UP
#define BUTTON_1_PULL_DOWN

#define BUTTON_2_PORT			GPIOB
#define BUTTON_2_PIN			GPIO_PIN_7
//#define BUTTON_2_PULL_UP
#define BUTTON_2_PULL_DOWN

//プロトタイプ宣言
button_e ButtonIsOn(int8_t);
button_e ButtonJudgePushTime(int8_t, uint32_t);

/*-----------------------------------------------
 * DIP Rotary Switch
 ----------------------------------------------*/
typedef enum {
	ROTARY_SW_MODE  = 1,
	ROTARY_SW_SPEED = 2
} rotary_sw_e;

//モード設定用
#define ROTARY_SW_MODE_BIT_1_PORT		GPIOH
#define ROTARY_SW_MODE_BIT_1_PIN		GPIO_PIN_1

#define ROTARY_SW_MODE_BIT_2_PORT		GPIOC
#define ROTARY_SW_MODE_BIT_2_PIN		GPIO_PIN_15

#define ROTARY_SW_MODE_BIT_4_PORT		GPIOC
#define ROTARY_SW_MODE_BIT_4_PIN		GPIO_PIN_14

#define ROTARY_SW_MODE_BIT_8_PORT		GPIOH
#define ROTARY_SW_MODE_BIT_8_PIN		GPIO_PIN_0

//スピード設定用
#define ROTARY_SW_SPEED_BIT_1_PORT		GPIOB
#define ROTARY_SW_SPEED_BIT_1_PIN		GPIO_PIN_9

#define ROTARY_SW_SPEED_BIT_2_PORT		GPIOD
#define ROTARY_SW_SPEED_BIT_2_PIN		GPIO_PIN_2

#define ROTARY_SW_SPEED_BIT_4_PORT		GPIOB
#define ROTARY_SW_SPEED_BIT_4_PIN		GPIO_PIN_3

#define ROTARY_SW_SPEED_BIT_8_PORT		GPIOC
#define ROTARY_SW_SPEED_BIT_8_PIN		GPIO_PIN_13

//プロトタイプ宣言
int8_t GetRotarySwitch(rotary_sw_e);

/*-----------------------------------------------
 * Field Switch
 ----------------------------------------------*/
//スライドスイッチのポート、ピン
#define FIELD_SW_PORT		GPIOA
#define FIELD_SW_PIN		GPIO_PIN_12

//プロトタイプ宣言
field_select_e GetFieldSW(void);

/*-----------------------------------------------
 * Field Indicator
 ----------------------------------------------*/
#define FIELD_IDC_RED_PORT	GPIOC
#define FIELD_IDC_RED_PIN	GPIO_PIN_9

#define FIELD_IDC_BLUE_PORT	GPIOA
#define FIELD_IDC_BLUE_PIN	GPIO_PIN_8

void IndicateField(field_select_e);

#ifdef __cplusplus
}
#endif
#endif /* USER_INTERFACE_H_ */
