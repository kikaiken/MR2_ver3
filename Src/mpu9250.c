/*
 * mpu9250.c
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#include "mpu9250.h"
#include "user_interface.h"
#include "constant.h"
#include "light_math.h"
#include <math.h>
#include <stdio.h>

//ファイル内グローバル変数
volatile static imu_s offset = {};
volatile static float angvel_resolution;
volatile static float acc_resolution;

//static関数プロトタイプ宣言
static int8_t MPU9250InitSub(gyro_range_s, acc_range_s);
static void MPU9250Send(uint8_t, uint8_t);
static uint8_t MPU9250Read(uint8_t);
static void MPU9250Select(void);
static void MPU9250OffsetWait(void);

/*
 * MPU92506軸読み取り
 * @param
 * @return
 */
void MPU9250GetData(imu_s *imu) {
	volatile uint8_t data_h[7] = {}, data_l[7] = {};
	volatile int16_t data[7] = {};

	//センサ : 6軸+温度
	for (volatile int8_t i = 0; i < 7; i++) {
		data_h[i] = MPU9250Read(MPU9250_ACCEL_XOUT_H + (2 * i));
		data_l[i] = MPU9250Read(MPU9250_ACCEL_XOUT_L + (2 * i));
		data[i] = (data_h[i] << 8) + data_l[i];
	}

	//加速度
	imu->acc_x = (data[0] * acc_resolution);
	imu->acc_y = (data[1] * acc_resolution);
	imu->acc_z = (data[2] * acc_resolution);

	//角速度
	imu->angvel_x = DEG2RAD(data[4] * angvel_resolution) - offset.angvel_x;
	imu->angvel_y = DEG2RAD(data[5] * angvel_resolution) - offset.angvel_y;
	imu->angvel_z = DEG2RAD(data[6] * angvel_resolution) - offset.angvel_z;

	//温度
	//めんどくせ
}

/*
 * 6軸のゼロ点を測定
 * @param
 * @return
 */
void MPU9250GetOffset(void) {
	imu_s data = {};
	imu_s sum = {};
	const uint16_t num_of_sample = 7777;

	//リセット
	offset.acc_x = offset.acc_y = offset.acc_z = 0;
	offset.angvel_x = offset.angvel_y = offset.angvel_z = 0;

	//警告音
	BuzzerOn();

	//num_of_sample回の測定の平均をとる
	for (int32_t i = 0; i < num_of_sample; i++) {
		MPU9250GetData(&data);
		sum.acc_x    += data.acc_x;
		sum.acc_y    += data.acc_y;
		sum.acc_z    += data.acc_z;
		sum.angvel_x += data.angvel_x;
		sum.angvel_y += data.angvel_y;
		sum.angvel_z += data.angvel_z;
		MPU9250OffsetWait();
	}
	//平均
	offset.acc_x    = sum.acc_x / num_of_sample;
	offset.acc_y    = sum.acc_y / num_of_sample;
	offset.acc_z    = sum.acc_z / num_of_sample;
	offset.angvel_x = sum.angvel_x / num_of_sample;
	offset.angvel_y = sum.angvel_y / num_of_sample;
	offset.angvel_z = sum.angvel_z / num_of_sample;

	//表示
	printf("MPU9250 : Offset Acquired\n");

	//警告音停止
	BuzzerOff();
}

/*
 * 初期設定複数回呼び出し
 * @param
 * @return
 * @note	電源投入直後は不安定になる可能性があるため
 */
int8_t MPU9250Init(gyro_range_s gyro_range, acc_range_s acc_range) {
	volatile int8_t check;
	volatile int8_t count = 0;

	printf("MPU9250 : Initializing...\n");

	//通信速度を落とす
	MPU9250_SPI_HANDLER.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
	HAL_SPI_Init(&MPU9250_SPI_HANDLER);

	HAL_Delay(10);
	do{
		check = MPU9250InitSub(gyro_range, acc_range);
		count++;
		if (count >= 5) {		//エラー
			printf("MPU9250 : Communication Error\n");
			return MY_ERROR;
		}
	} while (check != MY_ERROR_NONE);

	//分解能設定
	angvel_resolution = ((float)500 * powf(2, gyro_range)) / (float)65536;
	acc_resolution    = ((float)4 * GRAVITATIONAL_ACC * powf(2, acc_range)) / (float)65536;
	printf("MPU9250 : Gyro Resolution = %f\n", angvel_resolution);
	printf("MPU9250 : Acc  Resolution = %f\n", acc_resolution);

	//成功表示
	printf("MPU9250 : Communication Succeeded\n");

	//通信速度を上げる
	MPU9250_SPI_HANDLER.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
	HAL_SPI_Init(&MPU9250_SPI_HANDLER);

	//正常
	return MY_ERROR_NONE;
}

/*
 * 初期設定
 * @param
 * @return
 */
static int8_t MPU9250InitSub(gyro_range_s gyro_range, acc_range_s acc_range) {
	//WHO_AM_I
	uint8_t gyro_check;
	gyro_check = MPU9250Read(MPU9250_WHO_AM_I);
	printf("MPU9250 : WHO AM I = %d\n", gyro_check);

	//設定
	MPU9250Send(MPU9250_PWR_MGMT_1,   0x00);
	MPU9250Send(MPU9250_PWR_MGMT_2,   0b000000);		//6軸ENABLE
	MPU9250Send(MPU9250_INT_PIN_CFG,  0x02);
	MPU9250Send(MPU9250_GYRO_CONFIG,  gyro_range << 3);
	MPU9250Send(MPU9250_ACCEL_CONFIG, acc_range  << 3);

	if (gyro_check != 0x71){	//エラー
		return MY_ERROR;
	} else {					//正常
		return MY_ERROR_NONE;
	}
}

/*
 * データ書き込み
 * @param	address : MPU9250レジスタアドレス
 * 			data : 書き込むデータ
 * @return
 */
static void MPU9250Send(uint8_t address, uint8_t data) {
	uint8_t txdata[2] = {address, data};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(&MPU9250_SPI_HANDLER, &txdata[0], &rxdata[0], 1, 10);
	HAL_SPI_TransmitReceive(&MPU9250_SPI_HANDLER, &txdata[1], &rxdata[1], 1, 10);
	MPU9250Deselect();
}

/*
 * データ読み込み
 * @param	address : MPU9250レジスタアドレス
 * @return	読み込んだデータ
 */
static uint8_t MPU9250Read(uint8_t address) {
	uint8_t txdata[2] = {(address | 0x80), 0x00};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(&MPU9250_SPI_HANDLER, &txdata[0], &rxdata[0], 1, 10);
	HAL_SPI_TransmitReceive(&MPU9250_SPI_HANDLER, &txdata[1], &rxdata[1], 1, 10);
	MPU9250Deselect();
	return rxdata[1];
}

/*
 * チップセレクト
 * @param
 * @return
 */
static void MPU9250Select(void) {
	HAL_GPIO_WritePin(MPU9250_CS_PORT, MPU9250_CS_PIN, GPIO_PIN_RESET);
}

/*
 * チップセレクト解除
 * @param
 * @return
 */
void MPU9250Deselect(void) {
	HAL_GPIO_WritePin(MPU9250_CS_PORT, MPU9250_CS_PIN, GPIO_PIN_SET);
}

/*
 * オフセット用適当wait
 * @param
 * @return
 */
static void MPU9250OffsetWait(void) {
	volatile int32_t t = 500;
	while (t--);
}
