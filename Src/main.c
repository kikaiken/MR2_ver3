/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "constant.h"
#include "mode.h"
#include "user_interface.h"
#include "retarget.h"
#include "mpu9250.h"
#include "robotstate.h"
#include "leg.h"
#include "wall_sensor.h"
#include "light_math.h"
#include "path.h"
#include "gerge.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
sanddune_status_e sanddune_status;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	//TIM6 Interrupt : 1ms
	if (htim->Instance == TIM6) {
		static int32_t time = 0;
		imu_s imu;
		wall_sens_s wall_sens;
		wall_pos_s wall_pos;
		state_s ref_state;
		static pure_pursuit_status_e pp_status = PF_FOLLOWING;
		field_select_e field_color = GetFieldSW();
		float spd_duty = (GetRotarySwitch(ROTARY_SW_SPEED) + 1) * 0.10f;
		static int8_t tussock_status = 0;
		static mountain_status_e mountain_status;
		static int32_t mountain_top_flag = 0, mountain_top_time = 0;

		//各種センサデータ取得
		MPU9250GetData(&imu);
		LegGetData();
		WallSensorGetDist(&wall_sens);
		GetPosFromWall(&wall_pos, wall_sens);
		GetOdometry(imu);

		//経路追従制御
		switch (state.move_mode) {
		//スタートからサンドデューンの経路追従
		case MM_START_TO_SANDDUNE :
			//自己位置推定
			if (field_color == FIELD_RED) {
				state.odom.x = (state.odom.x * 0.93f) + (-4.5 - wall_sens.left_rear - (MR2_SENSOR_LFRF / 2)) * 0.07f;
			} else {
				state.odom.x = (state.odom.x * 0.93f) + (-(-4.5 - wall_sens.right_rear - (MR2_SENSOR_LFRF / 2))) * 0.07f;
			}
			//経路追従
			pp_status = CalcPurePursuit(&ref_state, field_color);
			MotorSetSpdDutyAngvel(spd_duty, ref_state.angvel);
			//経路追従終了
			if (pp_status == PF_FINISH) {
				sanddune_status = 0;
				SetMoveMode(MM_SANDDUNE_PREPARE);
			}
			break;

		//サンドデューンからタソックの経路追従
		case MM_SANDDUNE_TO_TUSSOCK :
			//経路追従
			pp_status = CalcPurePursuit(&ref_state, field_color);
			MotorSetSpdDutyAngvel(spd_duty, ref_state.angvel);
			//経路追従終了
			if (pp_status == PF_FINISH) {
				tussock_status = 0;
				SetMoveMode(MM_TUSSOCK_PREPARE);
			}
			break;

		//サンドデューン
		case MM_SANDDUNE :
			switch (sanddune_status) {
			case SANDDUNE_BEFORE_WALK :
				if (fabsf(RAD2DEG(state.angle.pitch)) < 6.0f) {MotorSetSpdDutyAngvel(0.22f, state.odom.theta * (-0.25f)); BuzzerOn();}
				else 										  {sanddune_status = SANDDUNE_GO_UP;}
				break;
			case SANDDUNE_GO_UP :
				if (fabsf(RAD2DEG(state.angle.pitch)) > 2.5f) {MotorSetSpdDutyAngvel4Sanddune(0.22f, state.odom.theta * (-0.25f)); BuzzerOff();}
				else 										  {sanddune_status = SANDDUNE_TOP;}
				break;
			case SANDDUNE_TOP :
				if (RAD2DEG(state.angle.pitch) > -10.0f)	  {MotorSetSpdDutyAngvel4Sanddune(0.3f, state.odom.theta * (-0.25f)); BuzzerOn();}
				else 									 	  {sanddune_status = SANDDUNE_GO_DOWN;}
				break;
			case SANDDUNE_GO_DOWN :
				if (wall_pos.rear_wall_dist > 0.50f) 		  {MotorSetSpdDutyAngvel(0.3f, state.odom.theta * (-0.25f)); BuzzerOff();}
				else 										  {sanddune_status = SANDDUNE_FINISH;}
				break;
			case SANDDUNE_FINISH :
				SetMoveMode(MM_SANDDUNE_TO_TUSSOCK_PREPARE);
				BuzzerOff();
				break;
			default : break;
			}
			break;

		//タソック
		case MM_TUSSOCK :
			if (RAD2DEG(state.odom.theta) > 20.0f) 			   {tussock_status = 1;}
			else if (RAD2DEG(state.odom.theta) < -20.0f)	   {tussock_status = -1;}
			else if (fabsf(RAD2DEG(state.odom.theta)) < 10.0f) {tussock_status = 0;}
			if (tussock_status == 0) {
				if (fabsf((wall_sens.left_rear + wall_sens.right_rear) - (1.25f - MR2_SENSOR_LFRF)) < 0.20f) {		//壁が見える状況・・・本番と状況が違うので注意(1.45f)
					MotorSetSpdDutyAngvel(0.35, state.odom.theta * (-0.25f) + (wall_sens.left_rear - wall_sens.right_rear) * 3);
				} else {
					MotorSetSpdDutyAngvel(0.35, state.odom.theta * (-0.25f));
				}
			} else if (tussock_status == 1) {
				MotorSetSpdDutyAngvel(0.0, -M_PI / 2);
			} else if (tussock_status == -1) {
				MotorSetSpdDutyAngvel(0.0, M_PI / 2);
			}
			break;

		//マウンテン
		case MM_MOUNTAIN :
			switch (mountain_status) {
			case MOUNTAIN_BEFORE_WALK :
				if (RAD2DEG(state.angle.pitch) < 8.0f) {
					MotorSetSpdDutyAngvel4Sanddune(0.20f, 0);
				} else {
					BuzzerOn();
					mountain_status = MOUNTAIN_CLIMBING;
				}
				break;
			case MOUNTAIN_CLIMBING :
				if (fabsf(RAD2DEG(state.angle.pitch)) > 4.0f) {
					if (field_color == FIELD_RED) {
						MotorSetSpdDutyAngvel4Sanddune(spd_duty, (state.odom.theta * (-0.25f)) + ((wall_sens.left_rear - 0.35f) * 0.5f));
					} else {
						MotorSetSpdDutyAngvel4Sanddune(spd_duty, (state.odom.theta * (-0.25f)) + ((0.35f - wall_sens.right_rear) * 0.5f));
					}
				} else {
					BuzzerOff();
					mountain_status = MOUNTAIN_TOP;
				}
				break;
			case MOUNTAIN_TOP :
				if (wall_pos.front_wall_dist > 0.70f) {
					if (field_color == FIELD_RED) {
						MotorSetSpdDutyAngvel4Sanddune(spd_duty, (state.odom.theta * (-0.25f)) + ((wall_sens.left_rear - 0.35) * 0.5f));
					} else {
						MotorSetSpdDutyAngvel4Sanddune(spd_duty, (state.odom.theta * (-0.25f)) + ((0.35f - wall_sens.right_rear) * 0.5f));
					}
				} else {
					mountain_top_flag = 1;
				}

				if (mountain_top_flag == 1) {mountain_top_time++;}
				else 						{mountain_top_time = 0;}

				if (mountain_top_time > 10) {
					uukhai_flag = 1;
					SetMoveMode(MM_STOP);
				}
				break;
			default : break;
			}
			break;

		//各モード準備
		case MM_START_TO_SANDDUNE_PREPARE :
			PrepareNextPath(field_color, wall_sens);
			break;
		case MM_SANDDUNE_TO_TUSSOCK_PREPARE :
			if (fabsf(wall_pos.rear_wall_dist - 0.38f) > 0.05f || fabsf(wall_pos.rear_wall_angle) > DEG2RAD(4)) {
				BackWallStick(wall_pos, 0.38f, 0);
			} else {
				PrepareNextPath(field_color, wall_sens);
			}
			break;
		case MM_SANDDUNE_PREPARE :
			switch (sanddune_status) {
			case 0 :
				if (field_color == FIELD_RED) {		//赤
					if (fabsf(RAD2DEG(fabsf(state.odom.theta) - DEG2RAD(310))) > 5.00f) {
						MotorSetSpdDutyAngvel(0, (state.odom.theta - DEG2RAD(310)) * (-13.00f));
					} else {
						sanddune_status = 1;
					}
				} else {	//青
					if (fabsf(RAD2DEG(fabsf(state.odom.theta) - DEG2RAD(230))) > 5.00f) {
						MotorSetSpdDutyAngvel(0, (state.odom.theta - DEG2RAD(230)) * (-13.00f));
					} else {
						sanddune_status = 1;
					}
				}
				break;
			default :
				state.odom.theta = state.angle.pitch = 0;
				sanddune_status = SANDDUNE_BEFORE_WALK;
				SetMoveMode(MM_SANDDUNE);
				break;
			}
			break;
		case MM_TUSSOCK_PREPARE :
			switch (tussock_status) {
			case 0 :
				if (field_color == FIELD_RED) {
					if (fabsf(RAD2DEG(fabsf(state.odom.theta) - DEG2RAD(260))) > 3.00f) {
//						MotorSetSpdDutyAngvel(0, (state.odom.theta - DEG2RAD(260)) * (-13.00f));
						MotorSetSpdTurnDuty(0.40, -0.40);
					} else {
						tussock_status = 1;
					}
				} else {
					if (fabsf(RAD2DEG(fabsf(state.odom.theta) - DEG2RAD(280))) > 3.00f) {
//						MotorSetSpdDutyAngvel(0, (state.odom.theta - DEG2RAD(280)) * (-13.00f));
						MotorSetSpdTurnDuty(0.40, 0.40);
					} else {
						tussock_status = 1;
					}
				}
				break;
			default :
				state.odom.theta = state.angle.pitch = 0;
				SetMoveMode(MM_TUSSOCK);
				break;
			}
			break;
		case MM_MOUNTAIN_PREPARE :
			state.odom.theta = state.angle.pitch = 0;;
			mountain_status = MOUNTAIN_BEFORE_WALK;
			mountain_top_time = 0;
			mountain_top_flag = 0;
			SetMoveMode(MM_MOUNTAIN);
			break;
		case MM_STOP :
			MotorSetSpdTurnDuty(0, 0);
			break;
		default : break;
		}

		if (time > 50) {
//			printf("%f %f %f\n", state.odom.x, state.odom.y, RAD2DEG(state.odom.theta));
//			printf("%f %f %f\n", RAD2DEG(state.angle.roll), RAD2DEG(state.angle.pitch), RAD2DEG(state.angle.yaw));
//			printf("%f %f\n", wall_sens.left_rear, wall_sens.right_rear);
//			printf("%f %f\n", wall_pos.front_wall_dist, wall_pos.front_wall_angle);
//			printf("%f\n", (state.odom.theta * (-0.05f)) + ((wall_sens.left_rear - 0.35f) * 6.0f));
			time = 0;
		} else {
			time += 10;
		}
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  volatile int8_t mode;
  volatile int8_t init_error;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_SPI3_Init();
  MX_TIM3_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  RetargetInit();	//setting stdio(printf / scanf)
  printf("\n\nMR2 : Initializing...\n");

  WallSensorEnable();

  init_error += MPU9250Init(MPU9250_GYRO_2000DPS, MPU9250_ACC_16G);		//MPU9250初期化
  init_error += AMT203Init();											//AMT203

  //エラー判定
  if (init_error == MY_ERROR_NONE) {
	  printf("MR2 : Initialize Succeeded\n");
  } else {
	  printf("MR2 : Initialize Failed\n");
	  UIWarning();
  }

  UIStartUp();

  //ジャイロのオフセット取得
  MPU9250GetOffset();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	//MODEロータリースイッチでモードを選択して、左のボタンを押すとmode.cの各関数を実行
	if (ButtonIsOn(2)) {
		ButtonJudgePushTime(2, 0);
		mode = GetRotarySwitch(ROTARY_SW_MODE);
		ExecMode(mode);
	}
	//フィールド色表示
	if (GetFieldSW() == FIELD_RED) {UILEDSet(0b0000); IndicateField(FIELD_RED);}
	else						   {UILEDSet(0b0001); IndicateField(FIELD_BLUE);}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 144;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
