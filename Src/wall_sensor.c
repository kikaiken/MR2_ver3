/*
 * wall_sensor.c
 *
 *  Created on: 2019/03/03
 *      Author: Ryohei
 */

#include "wall_sensor.h"
#include "user_interface.h"
#include "constant.h"
#include "mode.h"
#include <math.h>

//定数
#define ADC_DATA_BUFFER_SIZE	((uint32_t) 8)
#define MOVAVE_NUM				10

#define CONTACTLESS_SIG_THRES	0.30f

//ファイル内変数
static uint16_t adc_data[ADC_DATA_BUFFER_SIZE];			//センサ生値
static float wall_sens_movave[ADC_DATA_BUFFER_SIZE];	//センサ移動平均値

/*
 * 左右壁に対する横ずれ・前後壁に対する距離と角度を求める
 * @param
 * @return
 */
void GetPosFromWall(wall_pos_s *pos, wall_sens_s sens) {
	const float center_expect_long = (FIELD_WIDTH_UR1_TO_SAND - MR2_SENSOR_LFRF) / 2;	//中央にいた場合の左右壁までの距離
	//横ずれ
	pos->side_wall_error =  ((sens.left_front - center_expect_long) - (sens.right_front - center_expect_long)) / 2;	//左右センサから求めた距離の平均値をとる

	//前壁
	pos->front_wall_dist  = (sens.front_left + sens.front_right) / 2;
	pos->front_wall_angle = atan2f(sens.front_left - sens.front_right, MR2_SENSOR_FLFR);

	//後壁
	pos->rear_wall_dist   = (sens.rear_left + sens.rear_right) / 2;
	pos->rear_wall_angle  = atan2f(sens.rear_right - sens.rear_left, MR2_SENSOR_RLRR);
}

/*
 * 各壁センサのAD変換値を距離に変換して収納
 * @param
 * @return
 */
void WallSensorGetDist(wall_sens_s *wall_sens) {
	static int16_t wall_sens_buff[ADC_DATA_BUFFER_SIZE][MOVAVE_NUM] = {};
	static int8_t movave_index = 0;
	//移動平均
	for (int8_t i = 0; i < ADC_DATA_BUFFER_SIZE; i++) {
		wall_sens_buff[i][movave_index] = adc_data[i];
		wall_sens_movave [i] = 0;
	}
	for (int8_t i = 0; i < ADC_DATA_BUFFER_SIZE; i++) {
		for (int8_t j = 0; j < MOVAVE_NUM; j++) {
			wall_sens_movave[i] += wall_sens_buff[i][j];
		}
		wall_sens_movave[i] /= MOVAVE_NUM;
	}
	movave_index = (movave_index + 1) % MOVAVE_NUM;

	//移動平均AD変換値を距離に変換
	wall_sens->front_left  = 0.57708078f * powf(((float)wall_sens_movave[0] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->front_right = 0.57708078f * powf(((float)wall_sens_movave[1] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->left_front  = 0.57708078f * powf(((float)wall_sens_movave[2] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->left_rear   = 0.57708078f * powf(((float)wall_sens_movave[3] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->right_front = 0.57708078f * powf(((float)wall_sens_movave[4] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->right_rear  = 0.57708078f * powf(((float)wall_sens_movave[5] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->rear_left   = 0.57708078f * powf(((float)wall_sens_movave[6] * 3.3) / (float)4096, -1.07014571f);
	wall_sens->rear_right  = 0.57708078f * powf(((float)wall_sens_movave[7] * 3.3) / (float)4096, -1.07014571f);

	//非接触の合図用
	if (wall_sens->left_rear < CONTACTLESS_SIG_THRES || wall_sens->right_rear < CONTACTLESS_SIG_THRES) {contactless_sig = 1;}
	else																							   {contactless_sig = 0;}
}

/*
 * 壁センサAD変換・DMA転送開始
 * @param
 * @return
 */
void WallSensorEnable(void) {
	HAL_ADC_Start_DMA(&WALL_SENSOR_ADC_HANDLER, (uint32_t *)adc_data, ADC_DATA_BUFFER_SIZE);
}

/*
 * 壁センサAD変換・DMA転送停止
 * @param
 * @return
 */
void WallSensorDisable(void) {
	HAL_ADC_Stop_DMA(&WALL_SENSOR_ADC_HANDLER);
}
