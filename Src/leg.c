/*
 * motor.cpp
 *
 *  Created on: 2018/05/23
 *      Author: Ryohei
 */

#include "leg.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "constant.h"
#include "mpu9250.h"
#include "robotstate.h"

//定数
#define MOTOR_PWM_MAX 		(TIM_MOTOR_FREQ / MOTOR_PWM_FREQ);
#define ENABLE				true
#define DISABLE				false

#define LEG_ANGVEL_PGAIN	0.250f
#define LEG_ANGVEL_IGAIN	0.044f

#define LEG_MOVAVE_NUM		3

//壁貼り付きゲイン
#define WALL_STICK_SPEED_GAIN	2.15f
#define WALL_STICK_TURN_GAIN	1.23f

//ファイル内変数
static bool motor_enbl_dsbl = DISABLE;

//static関数プロトタイプ宣言
static void MotorSetDuty(float, float);
static void MotorSetPWM(int16_t, int16_t);
static void MotorSetDirection(int16_t, int16_t);
static void MotorEnable(void);
static int16_t AMT203GetPos(amt203_select_e);
static uint8_t AMT203ComByte(amt203_select_e, uint8_t);
static void AMT203ShortWait(void);
static void AMT203LongWait(void);

/*
 * 前壁貼り付き
 * @param
 * @return
 */
void FrontWallStick(wall_pos_s wall_pos, float len, float ang) {
	MotorSetSpdTurnDuty(fmaxf(fminf((wall_pos.front_wall_dist - len) * WALL_STICK_SPEED_GAIN, 0.40f), -0.40f),
			fmaxf(fminf((wall_pos.front_wall_angle - ang) * (-WALL_STICK_TURN_GAIN), 0.30f), -0.30f));
}

/*
 * 後壁貼り付き
 * @param
 * @return
 */
void BackWallStick(wall_pos_s wall_pos, float len, float ang) {
	MotorSetSpdTurnDuty(fmaxf(fminf((wall_pos.rear_wall_dist - len) * (-WALL_STICK_SPEED_GAIN), 0.40f), -0.40f),
			fmaxf(fminf((wall_pos.rear_wall_angle - ang) * (-WALL_STICK_TURN_GAIN), 0.30f), -0.30f));
}

/*
 * 角速度指令で動かす。速度はDuty比のまま
 * @param	duty_spd   : 並進成分Duty
 * 			ref_angvel : 角速度目標
 * @return
 */
void MotorSetSpdDutyAngvel(float duty_spd, float ref_angvel) {
	state.angvel_error_integ = fmaxf(fminf(state.angvel_error_integ + (ref_angvel - state.angvel), M_PI / 4), -M_PI / 4);
	MotorSetSpdTurnDuty(duty_spd, ((ref_angvel - state.angvel) * LEG_ANGVEL_PGAIN) + (state.angvel_error_integ * LEG_ANGVEL_IGAIN));
}

/*
 * 角速度指令で動かす。サンドデューン用
 * @param	duty_spd   : 並進成分Duty
 * 			ref_angvel : 角速度目標
 * @return
 */
void MotorSetSpdDutyAngvel4Sanddune(float duty_spd, float ref_angvel) {
	state.angvel_error_integ = fmaxf(fminf(state.angvel_error_integ + (ref_angvel - state.angvel), M_PI / 4), -M_PI / 4);
	MotorSetSpdTurnDuty(duty_spd, ((ref_angvel - state.angvel) * LEG_ANGVEL_PGAIN * 0.5) + (state.angvel_error_integ * LEG_ANGVEL_IGAIN * 0.5));
}

/*
 * 足回りモータに並進/回転成分でDutyを与える
 * @param	duty_spd  : 並進成分Duty
 * 			duty_turn : 回転成分Duty(左回りが正)
 * @return
 */
void MotorSetSpdTurnDuty(float duty_spd, float duty_turn) {
	MotorSetDuty(duty_spd - duty_turn, duty_spd + duty_turn);
}

/*
 * チェビシェフリンクモータを回す
 * @param	duty_l : 左モータのデューティ比(-1~1)
 * 			duty_r : 右モータのデューティ比(-1~1)
 * @return
 */
static void MotorSetDuty(float duty_l, float duty_r) {
	//DISABLE状態ならENABLEする
	if (motor_enbl_dsbl == DISABLE) {
		MotorEnable();
	}

	//Dutyを-MOTOR_DUTY_LIMIT~MOTOR_DUTY_LIMITの間に制限
	duty_l = fmaxf(fminf(duty_l, MOTOR_DUTY_LIMIT), -MOTOR_DUTY_LIMIT);
	duty_r = fmaxf(fminf(duty_r, MOTOR_DUTY_LIMIT), -MOTOR_DUTY_LIMIT);

	//コンペアマッチ値計算
	int16_t pwm_l = duty_l * MOTOR_PWM_MAX;
	int16_t pwm_r = duty_r * MOTOR_PWM_MAX;

	//出力
	MotorSetPWM(pwm_l, pwm_r);
}

/*
 * タイマにコンペアマッチ値をセット
 * @param	pwm_l : 左モータコンペアマッチ値
 * 			pwm_r : 右モータコンペアマッチ値
 * @return
 */
static void MotorSetPWM(int16_t pwm_l, int16_t pwm_r) {
	MotorSetDirection(pwm_l, pwm_r);
	pwm_l = abs(pwm_l);
	pwm_r = abs(pwm_r);
	__HAL_TIM_SetCompare(&H_TIM_MOTOR, TIM_MOTOR_L_CH, pwm_l);
	__HAL_TIM_SetCompare(&H_TIM_MOTOR, TIM_MOTOR_R_CH, pwm_r);
}

/*
 * チェビシェフリンクモータ回転方向設定
 * @param	pwm_l : 左モータコンペアマッチ値
 * 			pwm_r : 右モータコンペアマッチ値
 * @return
 */
static void MotorSetDirection(int16_t pwm_l, int16_t pwm_r) {
	if (pwm_l >= 0) {
		HAL_GPIO_WritePin(MOTOR_L1_PORT, MOTOR_L1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_L2_PORT, MOTOR_L2_PIN, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(MOTOR_L1_PORT, MOTOR_L1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_L2_PORT, MOTOR_L2_PIN, GPIO_PIN_SET);
	}

	if (pwm_r >= 0) {
		HAL_GPIO_WritePin(MOTOR_R1_PORT, MOTOR_R1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_R2_PORT, MOTOR_R2_PIN, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(MOTOR_R1_PORT, MOTOR_R1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_R2_PORT, MOTOR_R2_PIN, GPIO_PIN_SET);
	}
}

/*
 * ブレーキ
 * @param
 * @return
 */
void MotorBreak(void) {
	HAL_GPIO_WritePin(MOTOR_L1_PORT, MOTOR_L1_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_L2_PORT, MOTOR_L2_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_R1_PORT, MOTOR_R1_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOTOR_R2_PORT, MOTOR_R2_PIN, GPIO_PIN_SET);
	HAL_Delay(200);
	MotorDisable();
}

/*
 * チェビシェフリンクモータENABLE
 * @param
 * @return
 */
static void MotorEnable(void) {
	MotorSetPWM(0, 0);
	HAL_TIM_PWM_Start(&H_TIM_MOTOR, TIM_MOTOR_L_CH);
	HAL_TIM_PWM_Start(&H_TIM_MOTOR, TIM_MOTOR_R_CH);
	motor_enbl_dsbl = ENABLE;
}

/*
 * チェビシェフリンクモータDISABLE
 * @param
 * @return
 */
void MotorDisable(void) {
	MotorSetPWM(0, 0);
	HAL_TIM_PWM_Stop(&H_TIM_MOTOR, TIM_MOTOR_L_CH);
	HAL_TIM_PWM_Stop(&H_TIM_MOTOR, TIM_MOTOR_R_CH);
	motor_enbl_dsbl = DISABLE;
}


/*-----------------------------------------------
 * エンコーダ
 ----------------------------------------------*/
/*
 * 脚の位相や総回転角度、それらから機体のオドメトリ等を得る
 * @param
 * @return	正常:0 / エラー:-1
 */
int8_t LegGetData(void) {
	static int8_t n_call = 0;
	static float deg_l, deg_r;
	static float prev_deg_l = 0, prev_deg_r = 0;
	float diff_deg_l = 0, diff_deg_r = 0;
	static float vel_left = 0, vel_right = 0;
	//移動平均用
	static float vl_buff[LEG_MOVAVE_NUM] = {}, vr_buff[LEG_MOVAVE_NUM] = {};
	float vl_sum = 0, vr_sum = 0;
	static int8_t movave_index = 0;

	if (n_call < 10) {		//通信初期は読み取りデータが安定しないので機体情報を計算しない
		n_call++;
		deg_l = (float)(4096 - AMT203GetPos(AMT203_LEFT)) * 360.0f / 4096.0f;
		deg_r = (float)(AMT203GetPos(AMT203_RIGHT))       * 360.0f / 4096.0f;
		//エラー判定
		if (deg_l < 0 || deg_r < 0) {
			return MY_ERROR;
		}
	} else {				//呼び出し二回目以降
		prev_deg_l = deg_l;
		prev_deg_r = deg_r;
		deg_l = (float)(4096 - AMT203GetPos(AMT203_LEFT)) * 360.0f / 4096.0f;
		deg_r = (float)(AMT203GetPos(AMT203_RIGHT))       * 360.0f / 4096.0f;

		//エラー判定
		if (deg_l < 0 || deg_r < 0) {
			return MY_ERROR;
		}

		//左脚
		if ((deg_l - prev_deg_l) > 120.0f) {							//位相が0度から360度に飛んだとき
			diff_deg_l = (deg_l - 360.0f) + (0.0f - prev_deg_l);
		} else if (deg_l - prev_deg_l < -10.0f) {					//位相が360度から0度に飛んだとき
			diff_deg_l = (deg_l - 0.0f) + (360.0f - prev_deg_l);
		} else {
			diff_deg_l = deg_l - prev_deg_l;
		}
		//右脚
		if ((deg_r - prev_deg_r) > 120.0f) {							//位相が0度から360度に飛んだとき
			diff_deg_r = (deg_r - 360.0f) + (0.0f - prev_deg_r);
		} else if ((deg_r - prev_deg_r) < -10.0f) {					//位相が360度から0度に飛んだとき
			diff_deg_r = (deg_r - 0.0f) + (360.0f - prev_deg_r);
		} else {
			diff_deg_r = deg_r - prev_deg_r;
		}
	}

	//各脚の歩く速さ
	//移動平均
	vl_buff[movave_index] = (diff_deg_l / 360.0f) * MR2_STEP_LENGTH * 2;
	vr_buff[movave_index] = (diff_deg_r / 360.0f) * MR2_STEP_LENGTH * 2;
	for (int8_t i = 0; i < LEG_MOVAVE_NUM; i++) {
		vl_sum += vl_buff[i];
		vr_sum += vr_buff[i];
	}
	vel_left  = (vl_sum / LEG_MOVAVE_NUM) / CTRL_CYCLE;
	vel_right = (vr_sum / LEG_MOVAVE_NUM) / CTRL_CYCLE;
	movave_index = (movave_index + 1) % LEG_MOVAVE_NUM;			//インデックス更新

	//機体の速度
	state.vel = (vel_left + vel_right) / 2;

	//正常終了
	return MY_ERROR_NONE;
}

/*
 * 脚の位相の角度を読み出す
 * @param	lr : エンコーダ左右選択(AMT203_LEFT / AMT203_RIGHT)
 * @return
 */
int8_t LegGetPhaseDeg(int32_t *rot_l, float *deg_l, int32_t *rot_r, float *deg_r) {
	static int8_t n_call = 0;
	static int16_t pos_l, pos_r;
	static float prev_deg_l, prev_deg_r;
	if (n_call == 0) {
		n_call = 1;
		pos_l = 4096 - AMT203GetPos(AMT203_LEFT);
		pos_r = AMT203GetPos(AMT203_RIGHT);
		*deg_l = (float)pos_l * 360 / 4096;
		*deg_r = (float)pos_r * 360 / 4096;
	} else {
		prev_deg_l = (float)pos_l * 360 / 4096;
		prev_deg_r = (float)pos_r * 360 / 4096;
		pos_l = 4096 - AMT203GetPos(AMT203_LEFT);
		pos_r = AMT203GetPos(AMT203_RIGHT);
		*deg_l = (float)pos_l * 360 / 4096;
		*deg_r = (float)pos_r * 360 / 4096;

		if ((*deg_l - prev_deg_l) > 10) {			//左脚の位相が0度から360度に飛んだら回転数をインクリメント
			*rot_l -= 1;
		} else if (*deg_l - prev_deg_l < -10) {		//左脚の位相が360度から0度に飛んだら回転数をインクリメント
			*rot_l += 1;
		}
		if ((*deg_r - prev_deg_r) > 10) {			//右脚の位相が0度から360度に飛んだら回転数をインクリメント
			*rot_r -= 1;
		} else if ((*deg_r - prev_deg_r) < -10) {	//右脚の位相が360度から0度に飛んだら回転数をインクリメント
			*rot_r += 1;
		}
	}
	if (pos_l == -1 || pos_r == -1) {
		return -1;
	} else {
		return 0;
	}
}

/*
 * エンコーダ初期化
 * @param
 * @return
 * @note	電源投入直後初回通信時は値が変になることがあるので、まともな値がくるまで通信する
 */
int8_t AMT203Init(void) {
	float deg_l, deg_r;
	int32_t rot_l = 0, rot_r = 0;
	int8_t enc_status, enc_n_call = 0;
	const int8_t enc_n_call_limit = 100;

	printf("AMT203 : Initializing...\n");

	do {
		enc_status = LegGetPhaseDeg(&rot_l, &deg_l, &rot_r, &deg_r);
		enc_n_call++;
		HAL_Delay(1);
	} while (enc_status != 0 && enc_n_call < enc_n_call_limit);

	if (enc_status == -1) {
		printf("AMT203 : Communication Error\n");
		return MY_ERROR;
	} else {
		printf("AMT203 : Communication Succeeded\n");
		return MY_ERROR_NONE;
	}
}

/*
 * AMT203から位置を読み出す
 * @param	lr : エンコーダ左右選択(AMT203_LEFT / AMT203_RIGHT)
 * @return	受信した位置
 */
static int16_t AMT203GetPos(amt203_select_e lr) {
	uint8_t rxdata, pos_h, pos_l;

	//ダミー通信
	uint8_t dummy = 0x00;
	HAL_SPI_TransmitReceive(&AMT203_SPI_HANDLER, &dummy, &rxdata, 1, 10);
	AMT203ShortWait();

	//位置読み出し
	rxdata = AMT203ComByte(lr, AMT203_RD_POS);
	while (rxdata == 0xA5) {
		rxdata = AMT203ComByte(lr, AMT203_NOP_A5);
	};

	if (rxdata == AMT203_RD_POS) {
		pos_h = AMT203ComByte(lr, AMT203_NOP_A5);
		pos_l = AMT203ComByte(lr, AMT203_NOP_A5);
		//12bitデータを返す
		return (((pos_h & 0x0F) << 8) + pos_l);
	} else {
		//エラー
		return -1;
	}
}

/*
 * AMT203 8bit送受信
 * @param	txdata : 送信データ
 * @param	lr : エンコーダ左右選択(AMT203_LEFT / AMT203_RIGHT)
 * @return	受信データ
 */
static uint8_t AMT203ComByte(amt203_select_e lr, uint8_t txdata) {
	uint8_t rxdata;
	AMT203Select(lr);
	HAL_SPI_TransmitReceive(&AMT203_SPI_HANDLER, &txdata, &rxdata, 1, 10);
	AMT203ShortWait();
	AMT203Deselect();
	AMT203LongWait();
	return rxdata;
}

/*
 * AMT203チップセレクト
 * @param	lr : エンコーダ左右選択(AMT203_LEFT / AMT203_RIGHT)
 * @return
 */
void AMT203Select(amt203_select_e lr) {
	if (lr == AMT203_LEFT) {
		HAL_GPIO_WritePin(AMT203_LEFT_CS_PORT , AMT203_LEFT_CS_PIN , GPIO_PIN_RESET);
		HAL_GPIO_WritePin(AMT203_RIGHT_CS_PORT, AMT203_RIGHT_CS_PIN, GPIO_PIN_SET);
	} else if (lr == AMT203_RIGHT) {
		HAL_GPIO_WritePin(AMT203_LEFT_CS_PORT , AMT203_LEFT_CS_PIN , GPIO_PIN_SET);
		HAL_GPIO_WritePin(AMT203_RIGHT_CS_PORT, AMT203_RIGHT_CS_PIN, GPIO_PIN_RESET);
	}
}

/*
 * AMT203チップセレクト解除
 * @param
 * @return
 */
void AMT203Deselect(void) {
	HAL_GPIO_WritePin(AMT203_LEFT_CS_PORT , AMT203_LEFT_CS_PIN , GPIO_PIN_SET);
	HAL_GPIO_WritePin(AMT203_RIGHT_CS_PORT, AMT203_RIGHT_CS_PIN, GPIO_PIN_SET);
}

/*
 * AMT203短いウェイト : 約15us
 * @param
 * @return
 */
static void AMT203ShortWait(void) {
	volatile uint32_t i = 150;
	while (i--) ;
}

/*
 * AMT203長いウェイト : 約50us
 * @param
 * @return
 */
static void AMT203LongWait(void) {
	volatile uint32_t i = 500;
	while (i--) ;
}
