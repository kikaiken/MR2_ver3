/*
 * gerge.c
 *
 *  Created on: 2018/12/23
 *      Author: Ryohei
 */

#include "gerge.h"
#include "user_interface.h"
#include <math.h>

//static関数プロトタイプ宣言
static void GergeHandServoSetAngle(float);

/*
 * ゲルゲキャッチからのスタート
 * @param
 * @return
 */
void GergeCatchStart(void) {
	GergeMotorEnable();

	GergeHandServoTransfer();
	while (GergeIsToCatch() == 0);		//ハンドマイクロスイッチが反応するまで待ち
	GergeHandClose();

	//MR1が離すまで待つ
	HAL_Delay(500);
}

/*
 * ゲルゲキャッチ
 * @param
 * @return
 */
void GergeCatchAndWaitButton(void) {
	GergeHandServoTransfer();
	while (GergeIsToCatch() == 0);		//ハンドマイクロスイッチが反応するまで待ち
	GergeHandClose();
	while (ButtonIsOn(2) == BUTTON_OFF);
	ButtonJudgePushTime(2, 0);
}

/*
 * ゲルゲ持ち上げてウーハイ
 * @param
 * @return
 */
void GergeUukhai(void) {
	BuzzerOn();

	//ロック解除
	GergeUnLock();

	//ゲルゲの角度を垂直にする
	GergeHandServoUukhai();

	HAL_Delay(1500);

	//上げる
	GergeMotorEnable();
	GergeMotorSetDuty(1.0);
	HAL_Delay(2000);
	GergeMotorDisable();


	BuzzerOff();

	//ブザー
	UIUukhai();
}

/*
 * ゲルゲをボタンでロック。ロックされるまで警告音
 * @param
 * @return
 */
void GergeConfirmLock(void) {
	int16_t cfm_time = 0;
	while (ButtonIsOn(2) == BUTTON_OFF) {
		if (cfm_time < 100) {
			BuzzerOn();
		} else if (cfm_time < 500) {
			BuzzerOff();
		} else {
			cfm_time = 0;
		}
		cfm_time++;
		HAL_Delay(1);
	}
	BuzzerOff();
	ButtonJudgePushTime(2, 0);
	GergeLock();
}

/*
 * レールモータを指定したDutyで回す
 * @param	duty : モータに与えるDuty(-1 ~ 1)
 * @return
 */
void GergeMotorSetDuty(float duty) {
	//回転方向を指定
	if (duty >= 0) {
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_1_PORT, GERGE_MOTOR_DIR_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_2_PORT, GERGE_MOTOR_DIR_2_PIN, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_1_PORT, GERGE_MOTOR_DIR_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GERGE_MOTOR_DIR_2_PORT, GERGE_MOTOR_DIR_2_PIN, GPIO_PIN_SET);
	}
	//Duty制限をかけ絶対値をとる
	 duty = fabsf(fmaxf(fminf(duty, GERGE_DUTY_LIMIT), -GERGE_DUTY_LIMIT)) * (float)(GERGE_TIM_FREQ / GERGE_PWM_FREQ);

	//チャンネルにコンペアマッチ値を設定
	__HAL_TIM_SetCompare(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH, duty);
}

/*
 * レールモータENABLE
 * @param
 * @return
 */
void GergeMotorEnable(void) {
	GergeMotorSetDuty(0);
	HAL_TIM_PWM_Start(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH);
}

/*
 * レールモータDISABLE
 * @param
 * @return
 */
void GergeMotorDisable(void) {
	GergeMotorSetDuty(0);
	HAL_TIM_PWM_Stop(&GERGE_MOTOR_TIM_HANDLER, GERGE_MOTOR_TIM_CH);
}

/*
 * 受け渡し時のサーボ角度
 * @param
 * @return
 */
void GergeHandServoTransfer(void) {
	GergeHandServoSetAngle(-38);
}

/*
 * ウーハイ時のサーボ角度
 * @param
 * @return
 */
void GergeHandServoUukhai(void) {
	GergeHandServoSetAngle(0);
}

/*
 * ゲルゲサーボに角度指令
 * @param	angle_deg : 手先角度
 * @return
 */
static void GergeHandServoSetAngle(float angle_deg) {
	int16_t pulse = GERGE_HAND_SERVO_CENTER_VALUE + angle_deg * GERGE_HAND_SERVO_1DEG_VALUE;
	HAL_TIM_PWM_Start(&GERGE_SERVO_TIM_HANDLER, GERGE_HAND_SERVO_TIM_CH);
	__HAL_TIM_SetCompare(&GERGE_SERVO_TIM_HANDLER, GERGE_HAND_SERVO_TIM_CH, pulse);
}

/*
 * ゲルゲロック
 * @param
 * @return
 */
void GergeLock(void) {
	HAL_TIM_PWM_Start(&GERGE_SERVO_TIM_HANDLER, GERGE_LOCK_SERVO_TIM_CH);
	__HAL_TIM_SetCompare(&GERGE_SERVO_TIM_HANDLER, GERGE_LOCK_SERVO_TIM_CH, GERGE_LOCK_VALUE);
}

/*
 * ゲルゲアンロック
 * @param
 * @return
 */
void GergeUnLock(void) {
	HAL_TIM_PWM_Start(&GERGE_SERVO_TIM_HANDLER, GERGE_LOCK_SERVO_TIM_CH);
	__HAL_TIM_SetCompare(&GERGE_SERVO_TIM_HANDLER, GERGE_LOCK_SERVO_TIM_CH, GERGE_UNLOCK_VALUE);
}

/*
 * ゲルゲハンドを閉じる
 * @param
 * @return
 */
void GergeHandClose(void) {
	HAL_GPIO_WritePin(GERGE_HAND_CYLINDER_PORT, GERGE_HAND_CYLINDER_PIN, GPIO_PIN_SET);
}

/*
 * ゲルゲハンドを開く
 * @param
 * @return
 */
void GergeHandOpen(void) {
	HAL_GPIO_WritePin(GERGE_HAND_CYLINDER_PORT, GERGE_HAND_CYLINDER_PIN, GPIO_PIN_RESET);
}

/*
 * ゲルゲが掴める状態かどうか判定する
 * @param
 * @return	掴める:1/掴めない:0
 */
int8_t GergeIsToCatch(void) {
	if (HAL_GPIO_ReadPin(GERGE_TOUCH_SW_PORT, GERGE_TOUCH_SW_PIN) == GPIO_PIN_RESET) {return 1;}
	else                                                                           	 {return 0;}
}
