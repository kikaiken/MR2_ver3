/*
 * light_math.c
 *
 *  Created on: 2019/04/24
 *      Author: Ryohei
 */

/*
 * 符号を返す
 * @param	a : 値
 * @return	正 : 1 / 負 : -1 / ゼロ : 0
 */
float signf(float a) {
    if (a > 0) 	   return 1;
    else if(a < 0) return -1;
    else           return 0;
}



