/*
 * mode.c
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#include "mode.h"
#include "user_interface.h"
#include "mpu9250.h"
#include "wall_sensor.h"
#include "gerge.h"
#include "leg.h"
#include "robotstate.h"
#include "path.h"
#include "gerge.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

//マクロ
#define DEG2RAD(x)			(float)((x) * M_PI / 180)
#define RAD2DEG(x)			(float)((x) * 180 / M_PI)

//定数
#define AFTER_SANDDUNE_RESTART
//#define ON_SANDDUNE_RESTART

//グローバル変数
volatile int8_t contactless_sig = 0;
volatile int8_t uukhai_flag = 0;

/*
 * 一連動作
 */
void Mode0(void) {
	state.odom.theta = DEG2RAD(270);
	GergeCatchStart();

	SetMoveMode(MM_START_TO_SANDDUNE_PREPARE);
	HAL_TIM_Base_Start_IT(&htim6);

	//ゲルゲを少し引く
	GergeMotorSetDuty(-1.0);
	HAL_Delay(500);
	GergeMotorDisable();

	while (ButtonIsOn(2) == BUTTON_OFF);
	HAL_TIM_Base_Stop_IT(&htim6);
	MotorDisable();
	ButtonJudgePushTime(2, 0);
	BuzzerOff();
}

/*
 * ライン2から
 */
void Mode1(void) {
	GergeCatchAndWaitButton();

	state.odom.theta = DEG2RAD(270);
	SetMoveMode(MM_SANDDUNE_PREPARE);
	HAL_TIM_Base_Start_IT(&htim6);
	while (ButtonIsOn(2) == BUTTON_OFF);
	HAL_TIM_Base_Stop_IT(&htim6);
	MotorDisable();
	ButtonJudgePushTime(2, 0);
	BuzzerOff();
}

/*
 * ライン3から
 */
void Mode2(void) {
	GergeCatchAndWaitButton();

#ifdef AFTER_SANDDUNE_RESTART
	SetMoveMode(MM_SANDDUNE_TO_TUSSOCK_PREPARE);
#endif
#ifdef ON_SANDDUNE_RESTART
	state.odom.theta = state.angle.pitch = 0;
	SetMoveMode(MM_SANDDUNE);
	sanddune_status = SANDDUNE_TOP;
#endif
	HAL_TIM_Base_Start_IT(&htim6);
	while (ButtonIsOn(2) == BUTTON_OFF);
	HAL_TIM_Base_Stop_IT(&htim6);
	MotorDisable();
	ButtonJudgePushTime(2, 0);
	BuzzerOff();
}

/*
 * ウルトゥー2から
 */
void Mode3(void) {
	int32_t t = 0;
	GergeCatchAndWaitButton();

	SetMoveMode(MM_STOP);
	HAL_TIM_Base_Start_IT(&htim6);

	//非接触の合図待ち : LEDチカチカ
	while (contactless_sig == 0) {
		if (t > 100) {
			t = 0;
		} else if (t > 50) {
			IndicateField(FIELD_BLUE);
		} else {
			IndicateField(FIELD_RED);
		}
		t++;
		HAL_Delay(1);
	}

	//上り始める
	SetMoveMode(MM_MOUNTAIN_PREPARE);

	//ウーハイフラグもしくはボタン待ち
	while ((uukhai_flag == 0) && (ButtonIsOn(2) == BUTTON_OFF));
	HAL_TIM_Base_Stop_IT(&htim6);
	MotorDisable();
	ButtonJudgePushTime(2, 0);

	//ウーハイ
	if (uukhai_flag == 1) {GergeUukhai();}
	BuzzerOff();
}

/*
 * ウーハイのみ
 */
void Mode4(void) {
	GergeCatchAndWaitButton();
	GergeUukhai();
}

/*
 * タソック単体テスト
 */
void Mode5(void) {
	GergeCatchAndWaitButton();

	SetMoveMode(MM_TUSSOCK);
	HAL_TIM_Base_Start_IT(&htim6);
	while (ButtonIsOn(2) == BUTTON_OFF);
	HAL_TIM_Base_Stop_IT(&htim6);
	MotorDisable();
	ButtonJudgePushTime(2, 0);
}

/*
 * 足回りモータテスト
 */
void Mode6(void) {
	float speed = 0;
	while (ButtonIsOn(2) == BUTTON_OFF) {
		speed = (GetRotarySwitch(ROTARY_SW_SPEED) + 1) * 0.10f;
		MotorSetSpdTurnDuty(speed, 0);
	}
	MotorDisable();
	ButtonJudgePushTime(2, 0);
}

/*
 * エンコーダテスト
 */
void Mode7(void) {
	float deg_l, deg_r;
	int32_t rot_l = 0, rot_r = 0;
	printf("Encoder Test\n");
	while (ButtonIsOn(2) == BUTTON_OFF) {
		LegGetPhaseDeg(&rot_l, &deg_l, &rot_r, &deg_r);
		printf("AMT203 Left Leg = %f, Right Leg = %f\n", deg_l, deg_r);
		HAL_Delay(250);
	}
	ButtonJudgePushTime(2, 0);
}

/*
 * 壁センサテスト
 */
void Mode8(void) {
	static int32_t test_time = 0;
	wall_sens_s wall_sens;
	WallSensorEnable();
	while (ButtonIsOn(2) == BUTTON_OFF) {
		WallSensorGetDist(&wall_sens);
		HAL_Delay(1);
		if (test_time > 250) {
			printf("%f %f %f %f %f %f\n", wall_sens.front_left, wall_sens.front_right,
										  wall_sens.left_rear,  wall_sens.right_rear,
										  wall_sens.rear_left , wall_sens.rear_right);
			test_time = 0;
		} else {
			test_time++;
		}
	}
	ButtonJudgePushTime(2, 0);
}

/*
 * ジャイロテスト
 */
void Mode9(void) {
	imu_s imu;
	MPU9250GetOffset();
	while (ButtonIsOn(2) == BUTTON_OFF) {
		MPU9250GetData(&imu);
		printf("%f\n", state.odom.theta);
		HAL_Delay(500);
	}
	ButtonJudgePushTime(2, 0);
}

/*
 * モードを実行
 * @param	mode : 実行するモード番号
 * @return
 */
void ExecMode(int8_t mode) {
	//停止
	SetMoveMode(MM_STOP);
	//フィールド色選択
	field_select_e field_color = GetFieldSW();
	//音
	UIModeExec(field_color);

    switch (mode) {
    	case 0 : Mode0(); break;
        case 1 : Mode1(); break;
        case 2 : Mode2(); break;
        case 3 : Mode3(); break;
        case 4 : Mode4(); break;
        case 5 : Mode5(); break;
        case 6 : Mode6(); break;
        case 7 : Mode7(); break;
        case 8 : Mode8(); break;
        case 9 : Mode9(); break;
        default : break;
    }
}
