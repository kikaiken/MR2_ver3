/*
 * robotstate.c
 *
 *  Created on: 2018/06/21
 *      Author: Ryohei
 */

#include "robotstate.h"
#include "constant.h"
#include <math.h>

state_s state;
volatile float walk_speed;
float log4debug[1000];

/*
 * オドメトリを計算する
 * @param	imu : MPU9250のデータ
 * @return
 */
void GetOdometry(imu_s imu) {
//	static angle_s acc_ang = {};

	//水平面内での自己位置推定
	state.angvel = imu.angvel_z;
	state.odom.theta += state.angvel * CTRL_CYCLE;
	state.odom.x += state.vel * cosf(state.odom.theta) * CTRL_CYCLE;
	state.odom.y += state.vel * sinf(state.odom.theta) * CTRL_CYCLE;

	//機体の姿勢推定
	//加速度センサからロールとピッチの傾きを求める
	/*
	acc_ang.roll  = atan2f(imu.acc_x, imu.acc_z);
	acc_ang.pitch = atan2f(imu.acc_y, imu.acc_z);
	state.angle.roll  = acc_ang.roll;
	state.angle.pitch = acc_ang.pitch;
	*/

	//何故か加速度センサのY軸だけ値が吹っ飛んでるのでジャイロだけで角度推定
	state.angle.roll  += imu.angvel_y * CTRL_CYCLE;
	state.angle.pitch += imu.angvel_x * CTRL_CYCLE;
	state.angle.yaw   = state.odom.theta;

	/*
	//yaw軸回転を考慮
	state.angle.roll  += state.angle.pitch * sinf(imu.angvel_z * CTRL_CYCLE);
	state.angle.pitch -= state.angle.roll  * sinf(imu.angvel_z * CTRL_CYCLE);
	*/
}

void SetMoveMode(move_mode_e mode) {
	state.move_mode = mode;
}
