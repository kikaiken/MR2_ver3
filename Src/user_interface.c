/*
 * user_interface.c
 *
 *  Created on: 2018/05/20
 *      Author: Ryohei
 */


#include "user_interface.h"

/*-----------------------------------------------
 * 汎用タイマ
 ----------------------------------------------*/
/*
 * マイクロ秒タイマ
 * @param	t : 待ち時間(us)
 * @return
 */
/*
void DelayMicroSec(uint32_t t) {
	HAL_TIM_Base_Start_IT(&MICROSEC_TIM_HANDLER);
	while (t--) {
		while (__HAL_TIM_GET_FLAG(&MICROSEC_TIM_HANDLER, TIM_FLAG_UPDATE) == 0) ;
		__HAL_TIM_CLEAR_FLAG(&MICROSEC_TIM_HANDLER, TIM_FLAG_UPDATE);
	}
	HAL_TIM_Base_Stop_IT(&MICROSEC_TIM_HANDLER);
}
*/

/*-----------------------------------------------
 * LED・ブザー表示パターン
 ----------------------------------------------*/
/*
 * 警告表示LED
 * @param
 * @return
 */
void UIWarning(void) {
	for (int8_t i = 0; i < 5; i++) {
		UILEDSet(0b1111);
		BuzzerBeep(300);
		UILEDSet(0b0000);
		HAL_Delay(100);
	}
}

/*
 * モード決定
 * @param
 * @return
 */
void UIModeExec(field_select_e field_color) {
	if (field_color == FIELD_RED) {
		BuzzerBeep(250);
	} else {
		BuzzerBeep(100);
		HAL_Delay(50);
		BuzzerBeep(100);
	}
}

/*
 * モード設定失敗時に戻る用
 * @param
 * @return
 */
void UIModeBack(void) {

}

/*
 * 電源投入時
 * @param
 * @return
 */
void UIStartUp(void) {
	for (int8_t i = 0; i < 3; i++) {
		UILEDSet(0b1111);
		BuzzerBeep(80);
		UILEDSet(0b0000);
		HAL_Delay(40);
	}
}

/*
 * ウーハイ時
 * @param
 * @return
 */
void UIUukhai(void) {
	for (int8_t i = 0; i < 3; i++) {
		BuzzerBeep(50);
		HAL_Delay(50);
		BuzzerBeep(300);
		HAL_Delay(500);
	}
}


/*-----------------------------------------------
 * UILED
 ----------------------------------------------*/
/*
 * 指定にしたがってUILEDを点灯
 * @param	led : LEDを2進数の各ビットに見立てた時の値(ex:UILEDSet(0b1010))
 * @return
 */
void UILEDSet(uint8_t led) {
	HAL_GPIO_WritePin(UILED_BIT1_PORT, UILED_BIT1_PIN, ((led & 0b00000001) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT2_PORT, UILED_BIT2_PIN, ((led & 0b00000010) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT3_PORT, UILED_BIT3_PIN, ((led & 0b00000100) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT4_PORT, UILED_BIT4_PIN, ((led & 0b00001000) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
}

/*
 * 指定にしたがってUILEDをトグル
 * @param	tgl_led : 反転するLEDを1にする
 * @return
 */
void UILEDToggle(uint8_t tgl_led) {
	if (tgl_led & 0b00000001) {HAL_GPIO_TogglePin(UILED_BIT1_PORT, UILED_BIT1_PIN);}
	if (tgl_led & 0b00000010) {HAL_GPIO_TogglePin(UILED_BIT2_PORT, UILED_BIT2_PIN);}
	if (tgl_led & 0b00000100) {HAL_GPIO_TogglePin(UILED_BIT3_PORT, UILED_BIT3_PIN);}
	if (tgl_led & 0b00001000) {HAL_GPIO_TogglePin(UILED_BIT4_PORT, UILED_BIT4_PIN);}
}

/*-----------------------------------------------
 * Buzzer
 ----------------------------------------------*/
/*
 * ブザーを指定時間鳴らす
 * @param	beep_time : ブザーを鳴らす時間(ms)
 * @return
 */
void BuzzerBeep(int32_t beep_time) {
	BuzzerOn();
	HAL_Delay(beep_time);
	BuzzerOff();
}

/*
 * ブザーON
 * @param
 * @return
 */
void BuzzerOn(void) {
	HAL_GPIO_WritePin(BUZZER_PORT, BUZZER_PIN, GPIO_PIN_SET);
}

/*
 * ブザーOFF
 * @param
 * @return
 */
void BuzzerOff(void) {
	HAL_GPIO_WritePin(BUZZER_PORT, BUZZER_PIN, GPIO_PIN_RESET);
}

/*-----------------------------------------------
 * Button
 ----------------------------------------------*/
/*
 * ボタンのON/OFFを返す
 * @param
 * @return	PUSHSW_ON/PUSHSW_OFF
 */
button_e ButtonIsOn(int8_t num) {
	if (num == 1) {
#ifdef BUTTON_1_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_1_PORT, BUTTON_1_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_1_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON_1_PORT, BUTTON_1_PIN) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else if (num == 2) {
#ifdef BUTTON_2_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_2_PORT, BUTTON_2_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_2_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON_2_PORT, BUTTON_2_PIN) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else {
		return BUTTON_NUM_ERROR;
	}
}

/*
 * ボタンがOFFになるまで待って、長押し/短押しを返す
 * @param	long_short_thres : 長押し/短押しの閾値(ms)
 * @return	BUTTON_LONG/BUTTON_SHORT
 */
button_e ButtonJudgePushTime(int8_t num, uint32_t long_short_thres) {
	volatile uint32_t push_time = 100;				//押し時間タイマ
	HAL_Delay(push_time);							//チャタリング防止
	while (ButtonIsOn(num) == BUTTON_ON) {			//ボタンが押されている間カウント
		push_time++;
		HAL_Delay(1);
	}
	if (push_time > long_short_thres) return BUTTON_LONG;
	else							  return BUTTON_SHORT;
}

/*-----------------------------------------------
 * DIP Rotary Switch
 ----------------------------------------------*/
/*
 * DIPロータリースイッチの値を返す
 * @param	rotary_num : ロータリースイッチの名前
 * @return	ロータリースイッチの値 : 0 ~ 9
 */
int8_t GetRotarySwitch(rotary_sw_e num) {
	int8_t sw_data;
	if (num == ROTARY_SW_MODE) {
		sw_data =  (HAL_GPIO_ReadPin(ROTARY_SW_MODE_BIT_1_PORT, ROTARY_SW_MODE_BIT_1_PIN) * 1
				  + HAL_GPIO_ReadPin(ROTARY_SW_MODE_BIT_2_PORT, ROTARY_SW_MODE_BIT_2_PIN) * 2
				  + HAL_GPIO_ReadPin(ROTARY_SW_MODE_BIT_4_PORT, ROTARY_SW_MODE_BIT_4_PIN) * 4
				  + HAL_GPIO_ReadPin(ROTARY_SW_MODE_BIT_8_PORT, ROTARY_SW_MODE_BIT_8_PIN) * 8);
	} else if (num == ROTARY_SW_SPEED) {
		sw_data =  (HAL_GPIO_ReadPin(ROTARY_SW_SPEED_BIT_1_PORT, ROTARY_SW_SPEED_BIT_1_PIN) * 1
				  + HAL_GPIO_ReadPin(ROTARY_SW_SPEED_BIT_2_PORT, ROTARY_SW_SPEED_BIT_2_PIN) * 2
				  + HAL_GPIO_ReadPin(ROTARY_SW_SPEED_BIT_4_PORT, ROTARY_SW_SPEED_BIT_4_PIN) * 4
				  + HAL_GPIO_ReadPin(ROTARY_SW_SPEED_BIT_8_PORT, ROTARY_SW_SPEED_BIT_8_PIN) * 8);
	} else {
		sw_data = -1;
	}
	return sw_data;
}

/*-----------------------------------------------
 * Field Switch
 ----------------------------------------------*/
field_select_e GetFieldSW(void) {
	if (HAL_GPIO_ReadPin(FIELD_SW_PORT, FIELD_SW_PIN) == GPIO_PIN_SET) {return FIELD_RED;}
	else															   {return FIELD_BLUE;}
}

/*-----------------------------------------------
 * Field Indicator
 ----------------------------------------------*/
void IndicateField(field_select_e color) {
	if (color == FIELD_RED) {
		HAL_GPIO_WritePin(FIELD_IDC_RED_PORT,  FIELD_IDC_RED_PIN,  GPIO_PIN_SET);
		HAL_GPIO_WritePin(FIELD_IDC_BLUE_PORT, FIELD_IDC_BLUE_PIN, GPIO_PIN_RESET);
	} else if (color == FIELD_BLUE) {
		HAL_GPIO_WritePin(FIELD_IDC_RED_PORT,  FIELD_IDC_RED_PIN,  GPIO_PIN_RESET);
		HAL_GPIO_WritePin(FIELD_IDC_BLUE_PORT, FIELD_IDC_BLUE_PIN, GPIO_PIN_SET);
	}
}
